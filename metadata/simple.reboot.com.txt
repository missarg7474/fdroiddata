Categories:System
License:Apache-2.0
Web Site:
Source Code:https://github.com/franciscofranco/Simple-Reboot-app
Issue Tracker:https://github.com/franciscofranco/Simple-Reboot-app/issues

Auto Name:Simple Reboot
Summary:Reboot your device
Description:
Choose from reboot, soft reboot, or safe mode options.

Requires root: Yes.
.

Requires Root:yes

Repo Type:git
Repo:https://github.com/franciscofranco/Simple-Reboot-app

Build:4p11,11
    commit=cc232f89de13cec2f7425d942c0fb2bdb6a7c876
    subdir=app
    gradle=yes
    rm=app/libs/libsuperuser/build

Build:7.0,12
    commit=e707a11490ca0f5a81675069b688765ab270648c
    subdir=app
    gradle=yes
    forcevercode=yes
    prebuild=sed -i -e '/java.home/d' ../gradle.properties

Build:7.0,13
    commit=e707a11490ca0f5a81675069b688765ab270648c
    subdir=app
    gradle=yes
    forcevercode=yes
    prebuild=sed -i -e '/java.home/d' ../gradle.properties && \
        sed -i -e '/versionCode /s/getVersionCodeTimestamp()/$$VERCODE$$/g' build.gradle

Maintainer Notes:
Upstream now uses time-based version codes generated at build-time. Both would
break UCM on its own, so set this to static for now.

7.0 (13) is a workaround for a broken forcevercode=. Keep 7.0 (12) for
reference.
.

Auto Update Mode:None
Update Check Mode:Static
Current Version:7.0
Current Version Code:12
